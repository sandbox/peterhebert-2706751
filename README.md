# Navbar Extra Icons

The Navbar Extra Icons module adds extra icons for popular contrib modules.

## Icons

* Store (Drupal Commerce) - cart
* My Workbench (Workbench) - pencil
